<!DOCTYPE html>
<html lang="ru">
<head>
  <meta charset="UTF-8">
  <link rel="stylesheet" href="style.css">

  <link href="https://fonts.googleapis.com/css2?family=Noto+Sans&family=Poppins:wght@500&display=swap" rel="stylesheet"> 

  <title>Задание 3</title>
</head>
<body>
  <header>
    <div>
      <img id="logo" src="https://cdn.pixabay.com/photo/2020/06/09/19/47/squirrel-5279784__340.png" alt="Logo" style="width: 50px" />
      <h1 class="header">Some Random Website Name</h1>
    </div>
  </header>
  <div class="main">
    <section id="errors">
      <h2>Ошибка</h2>
      <?php 
        print($errorOutput);
      ?>
    </section>
  </div>
</body>
</html>
