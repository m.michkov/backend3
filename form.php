<!DOCTYPE html>
<html lang="ru">
<head>
  <meta charset="UTF-8">
  <link rel="stylesheet" href="style.css">

  <link href="https://fonts.googleapis.com/css2?family=Noto+Sans&family=Poppins:wght@500&display=swap" rel="stylesheet"> 

  <title>Задание 3</title>
</head>
<body>
  <header>
    <div>
      <img id="logo" src="https://cdn.pixabay.com/photo/2020/06/09/19/47/squirrel-5279784__340.png" alt="Logo" style="width: 50px" />
      <h1 class="header">Some Random Website Name</h1>
    </div>
  </header>
  <nav>
    <ul>
      <li><a href="#form" title = "Форма">Форма</a></li>
    </ul>
  </nav>
  <div class="main">
    <section id="form">
      <h2>Форма</h2>
      <form action="."
        method="POST">
        <label>
          Имя<br />
          <input name="name"
            value="Иван Иванович"/>
        </label><br />

        <label>
          E-mail:<br />
          <input name="email"
            value="test@example.com"
            type="email" />
        </label><br />

        <label>
          Дата рождения:<br />
          <input name="birthday"
            value="1970-01-01"
            type="date" />
        </label><br />

        Пол:<br />
        <label><input type="radio" checked="checked"
          name="gender" value="M" />
          мужской
        </label>
        <label><input type="radio"
          name="gender" value="F" />
          женский
        </label>
        <label><input type="radio"
          name="gender" value="O" />
          другое
        </label>
        <br />

        Количество конечностей:<br />
        <label><input type="radio"
          name="limbs" value="0" />
          0
        </label>
        <label><input type="radio"
          name="limbs" value="1" />
          1
        </label>
        <label><input type="radio"
          name="limbs" value="2" />
          2
        </label>
        <label><input type="radio"
          name="limbs" value="3" />
          3
        </label>
        <label><input type="radio" checked="checked"
          name="limbs" value="4" />
          4
        </label>
        <label><input type="radio"
          name="limbs" value="5" />
          5+
        </label>
        <br />

        <label>
          Сверхспособности:
          <br />
          <select name="superpowers[]"
            multiple="multiple">
            <option value="0">Бессмертие</option>
            <option value="1" selected="selected">Прохождение сквозь стены</option>
            <option value="2" selected="selected">Левитация</option>
          </select>
        </label><br />

        <label>
          Биография:<br />
          <textarea name="biography">Все началось, когда я родился...</textarea>
        </label><br />

        <br />
        <label><input type="checkbox"
          name="contract" />
          С контрактом ознакомлен
        </label><br />
        
        <input id="submit" type="submit" value="Отправить" />
      </form>
    </section>
  </div>
</body>
</html>
