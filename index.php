<?php
header('Content-Type: text/html; charset=UTF-8');

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  if (!empty($_GET['save'])) {
    print('Спасибо, результаты сохранены.');
  }
  include('form.php');
  exit();
}

$errors = FALSE;
$errorOutput = '';

$trimmedPost = [];
foreach ($_POST as $key => $value)
	if (is_string($value))
		$trimmedPost[$key] = trim($value);
	else
		$trimmedPost[$key] = $value;

if (empty($trimmedPost['name'])) {
  $errorOutput .= 'Заполните имя.<br/>';
  $errors = TRUE;
}
if (!preg_match('/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/', $trimmedPost['email'])) {
  $errorOutput .= 'Заполните email.<br/>';
  $errors = TRUE;
}
if (!preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $trimmedPost['birthday'])) {
  $errorOutput .= 'Заполните дату рождения.<br/>';
  $errors = TRUE;
}
if (!preg_match('/^[MFO]$/', $trimmedPost['gender'])) {
  $errorOutput .= 'Заполните пол.<br/>';
  $errors = TRUE;
}
if (!preg_match('/^[0-5]$/', $trimmedPost['limbs'])) {
  $errorOutput .= 'Заполните количество конечностей.<br/>';
  $errors = TRUE;
}
if (array_key_exists('superpowers', $trimmedPost)) {
  foreach ($trimmedPost['superpowers'] as $value)
    if (!preg_match('/[0-2]/', $value)) {
      $errorOutput .= 'Неверные суперспособности.<br/>';
      $errors = TRUE;
    }
}
if (!isset($trimmedPost['contract'])) {
  $errorOutput .= 'Вы не ознакомились с контрактом.<br/>';
  $errors = TRUE;
}

if ($errors) {
  include('errors.php');
  exit();
}

$user = 'u37069';
$pass = '1085747';
$db = new PDO('mysql:host=localhost;dbname=u37069', $user, $pass, array(PDO::ATTR_PERSISTENT => true));

try {
  $db->beginTransaction();
  $stmt1 = $db->prepare("INSERT INTO forms SET name = ?, email = ?, birthday = ?, 
    gender = ? , limb_number = ?, biography = ?");
  $stmt1 -> execute([$trimmedPost['name'], $trimmedPost['email'], $trimmedPost['birthday'], 
    $trimmedPost['gender'], $trimmedPost['limbs'], $trimmedPost['biography']]);
  $stmt2 = $db->prepare("INSERT INTO form_ability SET form_id = ?, ability_id = ?");
  $id = $db->lastInsertId();
  foreach ($trimmedPost['superpowers'] as $s)
    $stmt2 -> execute([$id, $s]);
  $db->commit();
}
catch(PDOException $e){
  $db->rollBack();
  $errorOutput = 'Error : ' . $e->getMessage();
  include('errors.php');
  exit();
}


header('Location: ?save=1');
